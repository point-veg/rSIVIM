
<!-- README.md is generated from README.Rmd. Please edit that file -->

# rSIVIM

<!-- badges: start -->
<!-- badges: end -->

This package contains functions to import XML files produced by SIVIM -
Sistema de Información de la Vegetación Ibérica y Macaronésica
(<http://www.sivim.info/sivi>) to R `data.frame`s (relevé table and the
respective header), using package `xml2`.

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/point-veg/rSIVIM")
```
