# get_xml_structure.R
#'
#' @title Gets the structure of the XML document
#'
#' @description This function accepts the output of the \code{\link[xml2]{read_xml}} function (from package `xml2`) and returns a `character vector` with all non-empty nodes and all the present node attribute names. This function uses the basic structure of the function \code{\link[xml2]{xml_structure}}  from `xml2` package.
#'
#' @param x `character` The output of the function \code{\link[xml2]{read_xml}} from package `xml2`, i.e. an object of class `xml_document`.
#' @param warn.empty.nodes `logical` If `FALSE` (the default) warns nothing; `TRUE` warns of empty nodes.
#'
#' @details The function lists in a `character vector` all non-empty nodes, showing the type of data contained in the node between `{}` and all the present node attribute names, between `[]`. The XML hierarchical structure is also presented, each level separated by `:`.
#'
#' @return A `character vector` with the XML structure (non-empty nodes and all the present node attribute names).
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tiagomonteirohenriques@@gmail.com}.
#'
#' @export
#'
get_xml_structure <- function (x, warn.empty.nodes=FALSE) {

  res <- NULL
  tmp_xml_structure <- function (x, parent="") {
    type <- xml2::xml_type(x)
    if (type == "element") {
      attr <- xml2::xml_attrs(x)
      if (length(attr) > 0) {
        attr_str <- paste0(" [", paste0(names(attr), collapse = ", "), "]") #attribute names, when present
      } else {attr_str <- ""}
      node <- paste0(parent, xml2::xml_name(x), ":", attr_str)
      parent <- paste0(parent, xml2::xml_name(x), ":")
      res <- c(res, node)
      res <- c(res, unlist(lapply(xml2::xml_contents(x), tmp_xml_structure, parent=parent))) #when xml_contents is list(), unlist returns NULL
      return(res)
    }
    else {
      res <- c(res, paste0(parent, "{", type, "}")) #content type, when present
      return(res)
    }
  }
  result <- unique(tmp_xml_structure(x))
  ind <- sapply(result, function (x) {sum(grepl(x, result, fixed=TRUE))==1})
  resind <- result[ind]
  t1 <- grep(".\\[", resind, value=TRUE)
  t2 <- sub("(.*) \\[(.*)\\].*", "\\1", t1)
  t3 <- sub("(.*) \\[(.*)\\].*", "\\2", t1)
  t4 <- strsplit(t3, ", ")
  t5 <- unique(unlist(mapply(paste, t2, t4, sep="[")))
  t6 <- paste0(t5, "]") #nodes with attributes
  t7 <- grep(".\\{", resind, value=TRUE) #nodes with content
  t8 <- grep(".\\[|.\\{", resind, value=TRUE, invert=TRUE) #empty nodes

  if (warn.empty.nodes & length(t8)>0) {warning("The following nodes seem empty:\n\n", paste0(paste0("\"",t8,"\""), collapse=" \n"), "\n", call.=FALSE)}
  return(c(t6,t7))
}
